CFLAGS = -std=c11 -D_XOPEN_SOURCE=700 -fPIC -g -O3 -Wall -fsanitize=thread
LIBS=-lm
LDFLAGS = -Wl,--version-script=libthreadpool.v -shared -pthread $(LIBS) -fsanitize=thread
CC = cc

TARGET = libthreadpool.so

OBJECTS =               \
    threadpool.o        \
    worker.o            \

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CC) ${LDFLAGS} -o $@ $(OBJECTS)

%.o: %.c
	$(CC) $(CFLAGS) -MMD -MF $(@:.o=.d) -MT $@ -c -o $@ $<

clean:
	-rm -f $(OBJECTS) $(TARGET)

-include $(OBJECTS:.o=.d)

.PHONY: clean
