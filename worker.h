/*
 * Copyright 2018 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This is the public API for interaction between the individual worker threads
 * and the pool.
 */

#ifndef TP_WORKER_H
#define TP_WORKER_H

typedef struct TPContext WorkSource;

typedef struct WorkBatch {
    int (*job_func)(void *arg, unsigned int job_idx, unsigned int thread_idx);
    void *func_arg;

    unsigned int job_idx_start;
    unsigned int job_idx_end;
    int finish;
} WorkBatch;

/**
 * Called by the worker with index worker_idx to wait for a new batch of jobs.
 *
 * @return 0 if wb has been successfully filled, a negative error otherwise
 */
int tpi_worksource_wait_jobs(WorkSource *ws, unsigned int worker_idx,
                             WorkBatch *wb);

/**
 * Steal a job from another worker.
 *
 * @return a non-negative job index if a job was successfully stolen
 * -1 if there are no more jobs available
 */
int tpi_worksource_steal_job(WorkSource *ws, unsigned int worker_idx);

typedef struct Worker Worker;

/**
 * Launch a new worker with the given unique index worker_idx.
 *
 * @return 0 if the worker has been launched, a negative error code otherwise
 */
int  tpi_worker_spawn(Worker **w, unsigned int worker_idx, WorkSource *ws);
void tpi_worker_destroy(Worker **w);

/**
 * Called by the work source to steal a job.
 *
 * @return a non-negative job index if a job was successfully stolen
 * -1 if there are no more jobs available.
 */
int tpi_worker_steal_job(Worker *w);

#endif // TP_WORKER_H
